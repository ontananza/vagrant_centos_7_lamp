# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Vagrant Centos/7 LAMP ###

* Script de inicialización y montaje de un LAMP con PHP 7.1 para un box Centos/7
* Version 1.0


### How do I get set up? ###

* Instalar Vagrant
* Agregar el box Centos/7
* Crear la carpeta del proyecto.
* vagrant up
* vagrant ssh
* Para probar la instalación http://192.168.10.10/info.php
* Adicionalmente


### Who do I talk to? ###

* Julio González <ontananza@msn.com>
