#!/usr/bin/env bash

BASE_ARCH='x86_64'
HOST_IP_ADDRESS='10.0.2.2'
HOST_ALTERNATE_IP_ADDRESS='192.168.10.1'
DATABASE_ROOT_PASS='vagrant'
DATABASE_USER='vagrant'
DATABASE_USER_PASSWORD='vagrant'

#Instalando
yum clean all
yum update -y
yum install -y wget vim epel-release
yum update -y

wget https://centos7.iuscommunity.org/ius-release.rpm
rpm -Uvh ius-release*.rpm

yum update -y

yum install -y httpd

systemctl enable httpd
systemctl start httpd

yum install -y php71u php71u-common php71u-cli php71u-bcmath php71u-gd php71u-pecl-apcu php71u-mbstring php71u-mcrypt php71u-mysqlnd php71u-pdo php71u-pecl-mongodb php71u-snmp php71u-soap php71u-xml


systemctl restart httpd

yum update -y


wget http://repo.mysql.com/yum/mysql-5.6-community/el/7/$BASE_ARCH/mysql-community-release-el7-5.noarch.rpm
rpm -ivh mysql-community-release-el7-5.noarch.rpm

yum update -y

yum install -y mysql-community-server mysql-community-client mysql-community-common mysql-community-libs mysql-utilities

systemctl enable mysqld

systemctl start mysqld

#Asegurando MySQL
mysqladmin -u root password "$DATABASE_ROOT_PASS"
mysql -u root -p"$DATABASE_ROOT_PASS" -e "UPDATE mysql.user SET Password=PASSWORD('$DATABASE_ROOT_PASS') WHERE User='root'"
mysql -u root -p"$DATABASE_ROOT_PASS" -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
mysql -u root -p"$DATABASE_ROOT_PASS" -e "DELETE FROM mysql.user WHERE User=''"
mysql -u root -p"$DATABASE_ROOT_PASS" -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'"
mysql -u root -p"$DATABASE_ROOT_PASS" -e "GRANT ALL PRIVILEGES ON *.* TO '$DATABASE_USER'@'$HOST_IP_ADDRESS' IDENTIFIED BY '$DATABASE_USER_PASSWORD'"
mysql -u root -p"$DATABASE_ROOT_PASS" -e "GRANT ALL PRIVILEGES ON *.* TO '$DATABASE_USER'@'$HOST_ALTERNATE_IP_ADDRESS' IDENTIFIED BY '$DATABASE_USER_PASSWORD'"
mysql -u root -p"$DATABASE_ROOT_PASS" -e "FLUSH PRIVILEGES"



mkdir /vagrant/www

chcon -R --reference=/var/www/ /vagrant/

cat > /etc/httpd/conf.d/host_vagrant.conf <<"EOF1"

Alias /ws/ "/vagrant/www/

<Directory "/vagrant/www">
    Options MultiViews FollowSymlinks
    AllowOverride Options FileInfo
    Require all granted
</Directory>

EOF1

cat > /var/www/html/info.php <<"EOF1"
<?php

phpinfo();

EOF1

systemctl restart httpd

#permitiendo que el httpd pueda conectarse a conexiones tcp!
setsebool -P httpd_can_network_connect 1
setsebool -P httpd_can_network_connect_db 1

#para permitir q apache escriba a alguna carpeta...
#chcon -t httpd_sys_rw_content_t ./hoteles/ ./tmp/

#Instalando mongodb
cat > /etc/yum.repos.d/mongodb-org-3.4.repo <<"EOF1"
[mongodb-org-3.4]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.4/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc

EOF1

sudo yum install -y mongodb-org

semanage port -a -t mongod_port_t -p tcp 27017

systemctl enable mongod
systemctl start mongod

